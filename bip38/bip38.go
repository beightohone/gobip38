// BIP38 PACKAGE
//
// - Encrypt compressed & uncompressed WIF key
// - Decrypt BIP38 secret to WIF key
// - Generate new BIP38 keys with EC Multiply mode
// - Generate new BIP38 key by generating new private/WIF keys

// @TODO:
//	1. Normalize password
//  2. Merge AES Encryption functions

package bip38

import (
	// "fmt"
	"bytes"
	"errors"
	"crypto/sha256"
	"crypto/aes"
	"crypto/rand"
	"math/big"
	"encoding/binary"
	"github.com/gcash/bchd/chaincfg"
	"github.com/gcash/bchd/bchec"
	"github.com/gcash/bchutil"
	"github.com/gcash/bchutil/base58"
	"golang.org/x/crypto/scrypt"
)

var (
	Base58DecodeError = errors.New("Could not decode encryped BIP38 key.")
	BIP38UnsupportedKey = errors.New("Unsupported BIP38 key. Encrypted key possibly corrupted.")
	BIP38ChecksumMismatch = errors.New("BIP38 checksum mismatch. Encrypted key possibly corrupted.")
	FaultyPassword = errors.New("Bitcoin address checksum mismatch. Faulty password!")
	PassfactorToPasspointError = errors.New("Could not derive passpoint from passfactor.")
	BigIntError = errors.New("Failed to create Int for N")
)

const PubKeyBytesLenCompressed = 33
const pubkeyCompressed byte = 0x2

// https://github.com/bitcoin/bips/blob/master/bip-0038.mediawiki#Encryption_when_EC_multiply_flag_is_not_used

// Encryption when EC multiply flag is not used:
//  1. Compute the Bitcoin address (ASCII), and take the first four bytes 
//     of SHA256(SHA256()) of it. Let's call this "addresshash".
//  2. Derive a key from the passphrase using scrypt
//      - Parameters: passphrase is the passphrase itself encoded in UTF-8 and normalized using 
//        Unicode Normalization Form C (NFC). salt is addresshash from the earlier step, n=16384,
//          r=8, p=8, length=64 (n, r, p are provisional and subject to consensus)
//      - Let's split the resulting 64 bytes in half, and call them derivedhalf1 and derivedhalf2.
//  3. Do AES256Encrypt(block = bitcoinprivkey[0...15] xor derivedhalf1[0...15], 
//     key = derivedhalf2), call the 16-byte result encryptedhalf1
//  4. Do AES256Encrypt(block = bitcoinprivkey[16...31] xor derivedhalf1[16...31], 
//     key = derivedhalf2), call the 16-byte result encryptedhalf2
//
//  The encrypted private key is the Base58Check-encoded concatenation of the following, 
//  which totals 39 bytes without Base58 checksum:
//
//    0x01 0x42 + flagbyte + salt + encryptedhalf1 + encryptedhalf2

// Encrypt WIF (prefix 0x42)
func Encrypt(wif *bchutil.WIF, password string) (string, error) {

	// Get the Public Key from WIF and generate a new pubkey interface
	pubKey, err := bchutil.NewAddressPubKey(wif.SerializePubKey(), &chaincfg.MainNetParams)
	if err != nil {
		return "", err
	}

	// Salt
	// Generate a 4-byte checksum of the Adress encoded from the Public Key
	// sha256(sha256(addr))
	sum := CheckSum([]byte(pubKey.EncodeAddress()))

	// Scrypt
	// Derive a key from the passphrase using scrypt
	key, err := scrypt.Key([]byte(password), sum, 16384, 8, 8, 64)
	if err != nil {
		return "", err
	}

	// Flag
	var flag byte = 0xC0
	if wif.CompressPubKey {
		flag = 0xE0
	}

	// AES
	data := encrypt(wif.PrivKey.Serialize(), key[:32], key[32:])
	buf := append([]byte{0x01, 0x42, flag}, sum...)
	buf = append(buf, data...)
	buf = append(buf, CheckSum(buf)...)
	//fmt.Println("buf:", buf)
	encryptWif := base58.Encode(buf)

	return encryptWif, nil
}

// Generate BIP38 secret from intermediate passphrase with EC multiply (prefix 0x43).
// BIP38 Printer code.
// https://github.com/bitcoin/bips/blob/master/bip-0038.mediawiki#encryption-when-ec-multiply-mode-is-used
func EncryptECMultiply(intermediatePassphrase string) (string, string, error) {

	// base58 decode intermediaPassphraseString
	decodedPass := base58.Decode(intermediatePassphrase)

	// Steps to create new encrypted private keys given intermediate_passphrase_string 
	// from owner (so we have ownerentropy, and passpoint, but we do not have passfactor 
	// or the passphrase): 

	// Prefix for ECM key
	prefix := []byte{0x01, 0x43}
	// compressed + lotsequence included
	flag := 0x24

	ownerEntropy := decodedPass[8:16]
	passpoint := decodedPass[16:49]

	seedb := make([]byte, 24)
	rand.Read(seedb)
	factorb := Hash256(seedb)
	
	// ECMultiply passpoint by factorb. Use the resulting EC point as a 
	// public key and hash it into a Bitcoin address
	const PubKeyBytesLenCompressed = 33
	const PubKeyBytesLenUncompressed = 65
	const pubkeyCompressed byte = 0x2
	const pubkeyUncompressed byte = 0x4

	// Decompress passpoint into pubkey(X, Y)
    var P PublicKey
    P.FromBytes(passpoint)

    // EC Multiply passpoint(X, Y) with factorb
	curve := bchec.S256()
	x, y := curve.ScalarMult(P.X, P.Y, factorb)

	// Encode address
	var pubKey PublicKey
	//pubKey.FromBytes(ecPointPubBytes)
	pubKey.X = x
	pubKey.Y = y
	addr := pubKey.ToAddress()

	// Address hash checksum
	addresshash := CheckSum([]byte(addr))

	derived, err := scrypt.Key(passpoint, append(addresshash, ownerEntropy...), 1024, 1, 1, 64)
	if err != nil {
		return "", "", err
	}

	derivedhalf1 := derived[:32]
	derivedhalf2 := derived[32:]

	// AES Encryption
	encryptedpart1, err := encryptECM(seedb[:16], derivedhalf1[:16] , derivedhalf2)
	if err != nil {
		return "", "", err
	}
	encryptedpart2, err := encryptECM(append(encryptedpart1[8:16], seedb[16:24]...), derivedhalf1[16:32], derivedhalf2)
	if err != nil {
		return "", "", err
	}

	// Serialize BIP38 key
	buf := append(prefix, byte(flag))
	buf = append(buf, addresshash...)
	buf = append(buf, ownerEntropy...)
	buf = append(buf, encryptedpart1[:8]...)
	buf = append(buf, encryptedpart2...)
	buf = append(buf, CheckSum(buf)...)

	// base58 encode BIP38 key
	encryptWif := base58.Encode(buf)


	return encryptWif, addr, nil
}

// Generate intermediate passphrase from password + salt
func GeneratePassphrase(password string, salt []byte) (string, error) {
	// The "lot" and "sequence" number are combined into a single 32 bit number. 
	// 20 bits are used for the lot number and 12 bits are used for the sequence number.
	// it is recommended that lot numbers be chosen at random within 
	// the range 100000-999999 and that sequence numbers are assigned starting with 1
	lotRand := make([]byte, 32)
	sequenceRand := make([]byte, 32)

	rand.Read(lotRand)
	rand.Read(sequenceRand)

	lot := binary.BigEndian.Uint32(lotRand)
	sequence := binary.BigEndian.Uint32(sequenceRand)

	lot = (lot % 899999) + 100000
	sequence = (sequence % 4095) + 1
	
	// 2. Encode the lot and sequence numbers as a 4 byte quantity (big-endian): 
	//    lotnumber * 4096 + sequencenumber.
	calc := lot * 4096 + sequence

	lotSequence := make([]byte, 4)
	binary.BigEndian.PutUint32(lotSequence, calc)

	// fmt.Println("lotSequence:", len(lotSequence), lotSequence)

	// 3. Concatenate ownersalt + lotsequence and call this ownerentropy.
	ownerEntropy := append(salt, lotSequence...)

	// fmt.Println("ownerEntropy:", len(ownerEntropy), ownerEntropy)

	// 4. Derive a key from the passphrase using scrypt
	//    Parameters: passphrase is the passphrase itself encoded in UTF-8 
	//				  and normalized using Unicode Normalization Form C (NFC). 
	//				  salt is ownersalt. n=16384, r=8, p=8, length=32.
	//	  Call the resulting 32 bytes prefactor.
	//	  Take SHA256(SHA256(prefactor + ownerentropy)) and call this passfactor. 
	//	  The "+" operator is concatenation.
	prefactor, err := scrypt.Key([]byte(password), salt, 16384, 8, 8, 32)
	if err != nil {
		return "", err
	}

	// fmt.Println("prefactor:", len(prefactor), prefactor)

	passfactor := Hash256(append(prefactor, ownerEntropy...))

	// fmt.Println("passfactor:", len(passfactor), passfactor)

	// 5. Compute the elliptic curve point G * passfactor, and convert the result 
	//	  to compressed notation (33 bytes). Call this passpoint. Compressed notation 
	//	  is used for this purpose regardless of whether the intent is to create 
	//	  Bitcoin addresses with or without compressed public keys.
	

	curve := bchec.S256()
	x, y := curve.ScalarBaseMult(passfactor)

	b := make([]byte, 0, PubKeyBytesLenCompressed)
	format := pubkeyCompressed
	if isOdd(y) {
		format |= 0x1
	}
	b = append(b, format)
	passpoint := paddedAppend(32, b, x.Bytes())

	// fmt.Println("passpoint:", len(passpoint), passpoint)

	// 6. Convey ownersalt and passpoint to the party generating the keys, 
	//    along with a checksum to ensure integrity.
	//		The following Base58Check-encoded format is recommended for this purpose: 
	//		magic bytes "2C E9 B3 E1 FF 39 E2 51" followed by ownerentropy, and then 
	//		passpoint. The resulting string will start with the word "passphrase" due 
	//		to the constant bytes, will be 72 characters in length, and 
	//		encodes 49 bytes (8 bytes constant + 8 bytes ownerentropy + 33 bytes 
	//		passpoint). The checksum is handled in the Base58Check encoding. 
	//		The resulting string is called intermediate_passphrase_string.
	magic := []byte{0x2C, 0xE9, 0xB3, 0xE1, 0xFF, 0x39, 0xE2, 0x51}

	intermediate := append(magic, ownerEntropy...)
	intermediate = append(intermediate, passpoint...)
	intermediate = append(intermediate, CheckSum(intermediate)...)

	intermediatePassphraseString := base58.Encode(intermediate)

	// If lot and sequence numbers are not being included, then follow the same 
	// procedure with the following changes:
	//		ownersalt is 8 random bytes instead of 4, and lotsequence is omitted. 
	//			ownerentropy becomes an alias for ownersalt.
	//		The SHA256 conversion of prefactor to passfactor is omitted. Instead, 
	//			the output of scrypt is used directly as passfactor.
	//		The magic bytes are "2C E9 B3 E1 FF 39 E2 53" instead (the last byte is 
	//			0x53 instead of 0x51).

	return intermediatePassphraseString, nil	

}

// Catchall Decrypt function
func Decrypt(encryptedKey string, password string) (*bchutil.WIF, error) {
	byteKey := base58.Decode(encryptedKey)
	if byteKey == nil {
		return nil, Base58DecodeError
	}
	identifier := byteKey[1]
	prefix := byteKey[0]&0x01 == 0x01
	known := identifier&0x42 == 0x42 || identifier&0x43 == 0x43
	ecm := identifier&0x43 == 0x43


	// Unsupported identifier or prefix
	if !prefix && !known {
		return nil, BIP38UnsupportedKey
	}

	// Non EC Multiplied key
	if !ecm {
		wif, err := DecryptNonECMultiplied(byteKey, password)
		if err != nil {
			return nil, err
		}

		return wif, nil
	// EC Multiplied key
	} else {
		wif, err := DecryptECMuliplied(byteKey, password)
		if err != nil {
			return nil, err
		}
		
		return wif, nil
	}
}

//  Decryption steps:
//
//  1.  Collect encrypted private key and passphrase from user.
//  2.  Derive derivedhalf1 and derivedhalf2 by passing the passphrase and addresshash 
//      into scrypt function.
//  3.  Decrypt encryptedhalf1 and encryptedhalf2 using AES256Decrypt, merge them to 
//      form the encrypted private key.
//  4.  Convert that private key into a Bitcoin address, honoring the compression 
//      preference specified in flagbyte of the encrypted key record.
//  5.  Hash the Bitcoin address, and verify that addresshash from the encrypted 
//      private key record matches the hash. If not, report that the passphrase 
//      entry was incorrect.

// Decrypt BIP38 byte slice which does not have the ECMultiply flag set
func DecryptNonECMultiplied(b []byte, password string) (*bchutil.WIF, error) {
	flag := b[2]
	hash := b[3:7]
	data := b[7:]

	// Verify checksum
	if !bytes.Equal(CheckSum(b[:len(b)-4]), data[len(data)-4:]) {
		return nil, BIP38ChecksumMismatch
	}

	dh, err := scrypt.Key([]byte(password), hash, 16384, 8, 8, 64)
	if err != nil {
		return nil, err
	}

	p := decrypt(data, dh[:32], dh[32:])
	privkey, _ := bchec.PrivKeyFromBytes(bchec.S256(), p)
	//privkey, pubkey := bchec.PrivKeyFromBytes(bchec.S256(), p)

	compress := false
	if flag == 0xE0 {
		compress = true
	}

	wif, err := bchutil.NewWIF(privkey, &chaincfg.MainNetParams, compress)
	if err != nil {
		return nil, err
	}

	// Generate pubkey to encode address for checksum
	pubKey, err := bchutil.NewAddressPubKey(wif.SerializePubKey(), &chaincfg.MainNetParams)
		if err != nil {
			return nil, err
	}

	if !bytes.Equal(hash, CheckSum([]byte(pubKey.EncodeAddress()))) {
		return nil, FaultyPassword
	}

	return wif, nil
}

// Decryption EC Multiply

// 1.  Collect encrypted private key and passphrase from user.
// 2.  Derive passfactor using scrypt with ownersalt and the user's 
//     passphrase and use it to recompute passpoint
// 3.  Derive decryption key for seedb using scrypt with passpoint, addresshash, 
//     and ownerentropy
// 4.  Decrypt encryptedpart2 using AES256Decrypt to yield the last 8 bytes of 
//     seedb and the last 8 bytes of encryptedpart1.
// 5.  Decrypt encryptedpart1 to yield the remainder of seedb.
// 6.  Use seedb to compute factorb.
// 7.  Multiply passfactor by factorb mod N to yield the private key associated 
//     with generatedaddress.
// 8.  Convert that private key into a Bitcoin address, honoring the compression 
//     preference specified in the encrypted key.
// 9.  Hash the Bitcoin address, and verify that addresshash from the encrypted 
//     private key record matches the hash. If not, report that the passphrase entry 
//     was incorrect.

// Decrypt BIP38 byte slice which has the ECMultiply flag set
func DecryptECMuliplied(b []byte, passphrase string) (*bchutil.WIF, error) {

	compress := b[2]&0x20 == 0x20
	hasLotSequence := b[2]&0x04 == 0x04

	var ownerSalt, ownerEntropy []byte
	if hasLotSequence {
		ownerSalt = b[7:11]
		ownerEntropy = b[7:15]
	} else {
		ownerSalt = b[7:15]
		ownerEntropy = ownerSalt
	}

	prefactorA, err := scrypt.Key([]byte(passphrase), ownerSalt, 16384, 8, 8, 32)
	if err != nil {
		return nil, err
	}

	var passFactor []byte
	if hasLotSequence {
		prefactorB := bytes.Join([][]byte{prefactorA, ownerEntropy}, nil)
		passFactor = Hash256(prefactorB)
	} else {
		passFactor = prefactorA
	}

	_, passpointPub := bchec.PrivKeyFromBytes(bchec.S256(), passFactor)

	var passpoint []byte
	if compress {
		passpoint = passpointPub.SerializeCompressed()
	} else {
		passpoint = passpointPub.SerializeUncompressed()
	}

	if passpoint == nil {
		return nil, PassfactorToPasspointError
	}

	encryptedpart1 := b[15:23]
	encryptedpart2 := b[23:39]

	derived, err := scrypt.Key(passpoint, bytes.Join([][]byte{b[3:7], ownerEntropy}, nil), 1024, 1, 1, 64)
	if err != nil {
		return nil, err
	}

	unencryptedpart2, err := decryptECM(encryptedpart2, derived[16:], derived[32:])
	if err != nil {
		return nil, err
	}

	encryptedpart1 = bytes.Join([][]byte{encryptedpart1, unencryptedpart2[:8]}, nil)

	unencryptedpart1, err := decryptECM(encryptedpart1, derived, derived[32:])
	if err != nil {
		return nil, err
	}

	seeddb := bytes.Join([][]byte{unencryptedpart1[:16], unencryptedpart2[8:]}, nil)
	factorb := Hash256(seeddb)

	bigN, success := new(big.Int).SetString("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141", 16)
	if !success {
		return nil, BigIntError
	}

	passFactorBig := new(big.Int).SetBytes(passFactor)
	factorbBig := new(big.Int).SetBytes(factorb)

	privKeyBI := new(big.Int)
	privKeyBI.Mul(passFactorBig, factorbBig)
	privKeyBI.Mod(privKeyBI, bigN)

	privKey, _ := bchec.PrivKeyFromBytes(bchec.S256(), privKeyBI.Bytes())

	wif, err := bchutil.NewWIF(privKey, &chaincfg.MainNetParams, compress)
	if err != nil {
		return nil, err
	}

	// Generate pubkey to encode address for checksum
	pubKey, err := bchutil.NewAddressPubKey(wif.SerializePubKey(), &chaincfg.MainNetParams)
		if err != nil {
			return nil, err
	}

	addr := pubKey.EncodeAddress()

	addrHashed := CheckSum([]byte(addr))

	if !bytes.Equal(addrHashed, b[3:7]) {
		return nil, FaultyPassword
	}

	return wif, nil
}

// AES256 Ecnryption
func encrypt(pk, f1, f2 []byte) []byte {
	c, _ := aes.NewCipher(f2)

	for i, _ := range f1 {
		f1[i] ^= pk[i]
	}

	dst := make([]byte, 48)
	c.Encrypt(dst, f1[:16])
	c.Encrypt(dst[16:], f1[16:])
	dst = dst[:32]

	return dst
}

// AES256 Encryption of EC multiplied keys
func encryptECM(pk, f1, f2 []byte) ([]byte, error) {
	h, err := aes.NewCipher(f2)
	if err != nil {
		return nil, err
	}

	for i := range pk {
		f1[i] ^= pk[i]
	}

	dst := make([]byte, 16)
	h.Encrypt(dst, f1)
	

	return dst, nil
}


// @TODO: Merge AES decryption functions
// AES256 Decryption
func decrypt(src, dh1, dh2 []byte) []byte {
	c, _ := aes.NewCipher(dh2)

	dst := make([]byte, 48)
	c.Decrypt(dst, src[:16])
	c.Decrypt(dst[16:], src[16:])
	dst = dst[:32]

	for i := range dst {
		dst[i] ^= dh1[i]
	}

	return dst
}

// AES256 Decryption of EC multiplied keys
func decryptECM(src, dh1, dh2 []byte) ([]byte, error) {
	h, err := aes.NewCipher(dh2)
	if err != nil {
		return nil, err
	}

	dst := make([]byte, 16)
	h.Decrypt(dst, src)
	for i := range dst {
		dst[i] ^= dh1[i]
	}

	return dst, nil
}

// Return full SHA256(SHA256()) hash
func Hash256(buf []byte) []byte {
	h := sha256.Sum256(buf)
	h = sha256.Sum256(h[:])
	return h[:]
}

// Return first 4 bytes of a SHA256(SHA256()) hash
func CheckSum(buf []byte) []byte {
	h := sha256.Sum256(buf)
	h = sha256.Sum256(h[:])
	return h[:4]
}

// paddedAppend appends the src byte slice to dst, returning the new slice.
// If the length of the source is smaller than the passed size, leading zero
// bytes are appended to the dst slice before appending src.
func paddedAppend(size uint, dst, src []byte) []byte {
	for i := 0; i < int(size)-len(src); i++ {
		dst = append(dst, 0)
	}
	return append(dst, src...)
}

func isOdd(a *big.Int) bool {
	return a.Bit(0) == 1
}